import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempDriveFormComponent } from './temp-drive-form.component';

describe('TempDriveFormComponent', () => {
  let component: TempDriveFormComponent;
  let fixture: ComponentFixture<TempDriveFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempDriveFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempDriveFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
