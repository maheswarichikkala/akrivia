import { Component, OnInit } from '@angular/core';
import {FormServiceService} from '../form-service.service'

@Component({
  selector: 'app-temp-drive-form',
  templateUrl: './temp-drive-form.component.html',
  styleUrls: ['./temp-drive-form.component.css']
})
export class TempDriveFormComponent implements OnInit {
  // model: any = {};
  rrfModel: any = {};
  constructor( private formServiceService:FormServiceService) { }

  ngOnInit() {
  }


  rrfsubmit(value  , fileid) {

    console.log(value)
    console.log(fileid)
    // console.log( this.imgResulturl);
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.rrfModel))
    let fileul=this.formServiceService.getFileUrl();
    // console.log(fileul);

  }


  public file: any;
  imgResulturl
  fileTextdata
fileError:boolean =false;
  reader(fileData) {

    this.file = fileData.target.files[0];
// type: "application/pdf"
    if (this.file && (this.file.type == "application/pdf" || this.file.type == "application/doc" || this.file.type == "application/docx")) {
      this.fileError=false
     

      console.log( this.file);

      let reader = new FileReader();

      reader.readAsDataURL(this.file);
     
      reader.onload = (event) => {

        console.log(reader.result);

        this.imgResulturl = (<FileReader>event.target).result;
        this.formServiceService.setFileUrl(this.imgResulturl);

        console.log( this.imgResulturl);
      }
    }
else{
  this.fileError=true
}

  }
}
