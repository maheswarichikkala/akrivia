 import { Component, OnInit } from '@angular/core';
  import {FormServiceService} from '../form-service.service'

  @Component({
    selector: 'app-temp-drive-form',
    templateUrl: './temp-drive-form.component.html',
    styleUrls: ['./temp-drive-form.component.css']
  })
  export class TempDriveFormComponent implements OnInit {
    // model: any = {};
    rrfModel: any = {};
    constructor( private formServiceService:FormServiceService) { }

    ngOnInit() {
    }


    rrfsubmit(value  , fileid) {

      console.log(value)
      console.log(fileid)
      // console.log( this.imgResulturl);
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.rrfModel))
      let fileul=this.formServiceService.getFileUrl();
      // console.log(fileul);

    }


    public file: any;
    imgResulturl
    fileTextdata
  fileError:boolean =false;
    reader(fileData) {

      this.file = fileData.target.files[0];
  // type: "application/pdf"
      if (this.file && (this.file.type == "application/pdf" || this.file.type == "application/doc" || this.file.type == "application/docx")) {
        this.fileError=false
      

        console.log( this.file);

        let reader = new FileReader();

        reader.readAsDataURL(this.file);
      
        reader.onload = (event) => {

          console.log(reader.result);

          this.imgResulturl = (<FileReader>event.target).result;
          this.formServiceService.setFileUrl(this.imgResulturl);

          console.log( this.imgResulturl);
        }
      }
  else{
    this.fileError=true
  }

    }
  }
  
  
  
  
  
  
  
  
  <div class="jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <form name="form" (ngSubmit)="f.form.valid &&  rrfsubmit()" #f="ngForm" novalidate>
                    <div class="form-group">
                        <label for="firstName">First Name</label>

                        <input type="text" class="form-control" name="firstName" [(ngModel)]="rrfModel.firstName"
                            #firstName="ngModel" required />
                        <div *ngIf=" firstName.invalid && firstName.touched">
                            <div class="text-danger">First Name is required</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lastName">Last Name</label>
                        <input type="text" class="form-control" name="lastName" [(ngModel)]="rrfModel.lastName"
                            #lastName="ngModel" required />
                        <div *ngIf="lastName.invalid" class="invalid-feedback">
                            <div *ngIf="lastName.errors.required">Last Name is required</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" name="email" [(ngModel)]="rrfModel.email"
                            #email="ngModel" required email />
                        <div *ngIf=" email.invalid" class="invalid-feedback">
                            <div *ngIf="email.errors.required">Email is required</div>
                            <div *ngIf="email.errors.email">Email must be a valid email address</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" [(ngModel)]="rrfModel.password"
                            #password="ngModel" required minlength="6" />
                        <div *ngIf=" password.invalid" class="invalid-feedback">
                            <div *ngIf="password.errors.required">Password is required</div>
                            <div *ngIf="password.errors.minlength">Password must be at least 6 characters</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" [(ngModel)]="rrfModel.password"
                            #password="ngModel" required minlength="6" />
                        <div *ngIf=" password.invalid" class="invalid-feedback">
                            <div *ngIf="password.errors.required">Password is required</div>
                            <div *ngIf="password.errors.minlength">Password must be at least 6 characters</div>
                        </div>
                    </div>


                    <!-- <input type="file" name="upload" accept="application/pdf,application/doc,application/docx "
                        (change)=reader($event)  [(ngModel)]="rrfModel.file" #fileid> -->

                        <input type="file" name="upload" 
                        (change)=reader($event)  [(ngModel)]="rrfModel.file" #fileid>
                        <div *ngIf="this.fileError">
                         <p class="text-danger">You must choose PDF or doc </p>
                        </div>

                    <div class="form-group">
                        <button (click)="rrfsubmit(f.value, fileid.value)" class="btn btn-primary">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>





import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FormServiceService {
public fileData;
  constructor() { }


  setFileUrl(fileData){
    this.fileData=fileData
  }

  getFileUrl()
  {
    console.log(this.fileData);
    return this.fileData
  }
}
